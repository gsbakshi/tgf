# TGF

Language :- Dart
Front-end Framework :- Flutter
Back-end Framework :- OpenCart
Scope :- This is a hybrid application (build on IOS and Android), which will facilitate meal and grocery orders with multiple types like Dine-In, Delivery and Take Away, through online Payment or COD.
We have different types of roles which are defined as follows:-
a. Admin
b. Store - manager
c. Kitchen- manager
d. Users
This application facilitates the users to order their meal and grocery.
This App will cover the following features:

Users have multiple ways to make payment either it be Cash On Delivery (COD) or Online Payment.
It offers user to search among multiple categories and food items.
Guest login: One can view the item details, its pricing being a guest.
Signup through social media eg: Google plus and Facebook
Referral code functionality: User can share their referral code with anyone and that person can register itself using referral code to get the exciting benefits.
Offers module: User will be provided with multiple different coupons to incur an advantage. Coupon will be of two types: User specific and other coupon which will be applicable to all users.
Notifications: User will be notified at every stage of it’s order. For eg: He will receive push notifications when his/her order gets moved to next stage like from Waiting to Processing or from Processing to Done.
Order History: Order History will be maintained for food and grocery both.
Contact Us: Contact Us section is provided, in case the user has any sort of query.
Help section is provided.
User can view and edit his/ her profile details.
User can manage, that is they can select, edit, modify, delete their address according as per requirement.

Broad classification of App

Food
Grocery

Food Module
Food Module will cover food items which will include Breakfast, Lunch and Dinner.

Food items are classified according to the category and sub-category.
User is provided with below mentioned options:

Set meal
Ala cart


Item customization is available according to the following options:

Upsized
Add-ons
Special request
Cup side
Standing side


This app allow three ways of ordering any food item, that will be listed below:

Take Away
Dine-In
Delivery



Grocery Module
Grocery Module will cover the grocery items along with household products.

Grocery Items will be available in grocery section of the App.
Grocery will further have 2 section of order type, that will be as follows:

Take away
Delivery
User can order any item from any of the types.



Backend
Backend will be bifurcated in the following different roles:

Administration: Super most authority will all rights.
Kitchen Manager: Will look after the processing of food orders.
Store Manager: Have access to food and grocery both.

Backend is based on OpenCart.
All categories, food items, grocery products, coupons will be created and managed by the backend. Admin and Store manager can view all the orders along with their details and have permission to change the status of the order.
Test Credentails
Email:  test@yopmail.com
password: 12345678

Note: Feel free to contact for further insights.